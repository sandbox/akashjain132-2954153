<?php

/**
 * @file
 * Provides the administrative interface for SMS Counter.
 */

/**
 * Form constructor for the SMS Counter's settings form.
 */
function smscounter_admin_settings($form, &$form_state) {

  $displays = _smscounter_display();

  $form['smscounter_display'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display'),
    '#options' => _smscounter_display(),
    '#description' => t('The number of item that will display at the bottom of textarea.'),
    '#default_value' => variable_get('smscounter_display', array_keys($displays)),
  );

  $form['smscounter_js_compression'] = array(
    '#type' => 'radios',
    '#title' => t('JS compression level'),
    '#options' => array(
      'min' => t('Production (minified)'),
      'none' => t('Development (uncompressed)'),
    ),
    '#default_value' => variable_get('smscounter_js_compression', 'min'),
  );

  return system_settings_form($form);
}
